/*
 * alchemist - code generation library
 * Copyright (C) 2008  Robbert Haarman
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>

#define CG_X86_OP_ADD 1
#define CG_X86_OP_ADC 2
#define CG_X86_OP_AND 3
#define CG_X86_OP_CALL 4
#define CG_X86_OP_CLC 5
#define CG_X86_OP_CLD 6
#define CG_X86_OP_CLI 7
#define CG_X86_OP_CMP 8
#define CG_X86_OP_DEC 9
#define CG_X86_OP_DIV 10
#define CG_X86_OP_HLT 11
#define CG_X86_OP_IDIV 12
#define CG_X86_OP_IMUL 13
#define CG_X86_OP_IN 14
#define CG_X86_OP_INC 15
#define CG_X86_OP_INSB 16
#define CG_X86_OP_INSD 17
#define CG_X86_OP_INSW 18
#define CG_X86_OP_INT 19
#define CG_X86_OP_INT1 20
#define CG_X86_OP_INT3 21
#define CG_X86_OP_INTO 22
#define CG_X86_OP_IRET 23
#define CG_X86_OP_IRETD 24
#define CG_X86_OP_IRETW 25
#define CG_X86_OP_JA 26
#define CG_X86_OP_JB 27
#define CG_X86_OP_JCXZ 28
#define CG_X86_OP_JECXZ 29
#define CG_X86_OP_JG 30
#define CG_X86_OP_JL 31
#define CG_X86_OP_JNA 32
#define CG_X86_OP_JNB 33
#define CG_X86_OP_JNG 34
#define CG_X86_OP_JNL 35
#define CG_X86_OP_JNO 36
#define CG_X86_OP_JNP 37
#define CG_X86_OP_JNS 38
#define CG_X86_OP_JNZ 39
#define CG_X86_OP_JO 40
#define CG_X86_OP_JP 41
#define CG_X86_OP_JS 42
#define CG_X86_OP_JZ 43
#define CG_X86_OP_JMP 44
#define CG_X86_OP_LDS 45
#define CG_X86_OP_LES 46
#define CG_X86_OP_LFS 47
#define CG_X86_OP_LGS 48
#define CG_X86_OP_LSS 49
#define CG_X86_OP_LEA 50
#define CG_X86_OP_LEAVE 51
#define CG_X86_OP_LGDT 52
#define CG_X86_OP_LIDT 53
#define CG_X86_OP_LLDT 54
#define CG_X86_OP_LMSW 55
#define CG_X86_OP_LODSB 56
#define CG_X86_OP_LODSD 57
#define CG_X86_OP_LODSW 58
#define CG_X86_OP_LOOP 59
#define CG_X86_OP_LOOPNZ 60
#define CG_X86_OP_LOOPZ 61
#define CG_X86_OP_LSL 62
#define CG_X86_OP_LTR 63
#define CG_X86_OP_MOV 64
#define CG_X86_OP_MOVSB 65
#define CG_X86_OP_MOVSD 66
#define CG_X86_OP_MOVSW 67
#define CG_X86_OP_MUL 68
#define CG_X86_OP_NEG 69
#define CG_X86_OP_NOP 70
#define CG_X86_OP_NOT 71
#define CG_X86_OP_OR 72
#define CG_X86_OP_OUT 73
#define CG_X86_OP_PAUSE 74
#define CG_X86_OP_POP 75
#define CG_X86_OP_POPAD 76
#define CG_X86_OP_POPAW 77
#define CG_X86_OP_POPFD 78
#define CG_X86_OP_POPFW 79
#define CG_X86_OP_PUSH 80
#define CG_X86_OP_PUSHAD 81
#define CG_X86_OP_PUSHAW 82
#define CG_X86_OP_PUSHFD 83
#define CG_X86_OP_PUSHFW 84
#define CG_X86_OP_RET 85
#define CG_X86_OP_RETF 86
#define CG_X86_OP_ROL 87
#define CG_X86_OP_ROR 88
#define CG_X86_OP_SHL 89
#define CG_X86_OP_SHR 90
#define CG_X86_OP_SUB 91
#define CG_X86_OP_XCHG 92
#define CG_X86_OP_XOR 93

#define CG_X86_REG_AX 1
#define CG_X86_REG_BX 2
#define CG_X86_REG_CX 3
#define CG_X86_REG_DX 4
#define CG_X86_REG_DI 5
#define CG_X86_REG_SI 6
#define CG_X86_REG_BP 7
#define CG_X86_REG_SP 8
#define CG_X86_REG_AL 9
#define CG_X86_REG_BL 10
#define CG_X86_REG_CL 11
#define CG_X86_REG_DL 12
#define CG_X86_REG_AH 13
#define CG_X86_REG_BH 14
#define CG_X86_REG_CH 15
#define CG_X86_REG_DH 16
#define CG_X86_REG_EAX 17
#define CG_X86_REG_EBX 18
#define CG_X86_REG_ECX 19
#define CG_X86_REG_EDX 20
#define CG_X86_REG_EDI 21
#define CG_X86_REG_ESI 22
#define CG_X86_REG_EBP 23
#define CG_X86_REG_ESP 24
#define CG_X86_REG_CS 25
#define CG_X86_REG_DS 26
#define CG_X86_REG_ES 27
#define CG_X86_REG_FS 28
#define CG_X86_REG_GS 29

#define CG_X86_MODE_32BIT 0
#define CG_X86_MODE_16BIT 1

#define CG_X86_ERR_INVALID_INSTRUCTION "invalid instruction"
#define CG_X86_ERR_INVALID_SCALE_FACTOR "invalid scale factor (must be 0, 1, 2, 4 or 8)"
#define CG_X86_ERR_UNKNOWN_OPCODE "unknown opcode"

int cg_x86_emit_nullary_instr(char *str, size_t size, int opcode);
int cg_x86_emit_imm8_instr(char *str, size_t size, int opcode, int imm);
int cg_x86_emit_mem16_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement);
int cg_x86_emit_mem32_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement);
int cg_x86_emit_mem8_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement);
int cg_x86_emit_mem16_imm16_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement, int imm);
int cg_x86_emit_mem32_imm32_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement, int imm);
int cg_x86_emit_mem8_imm8_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement, int imm);
int cg_x86_emit_mem16_reg16_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement, int reg);
int cg_x86_emit_mem32_reg32_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement, int reg);
int cg_x86_emit_mem8_reg8_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement, int reg);
int cg_x86_emit_reg16_instr(char *str, size_t size, int opcode, int reg);
int cg_x86_emit_reg32_instr(char *str, size_t size, int opcode, int reg);
int cg_x86_emit_reg8_instr(char *str, size_t size, int opcode, int reg);
int cg_x86_emit_reg16_imm16_instr(char *str, size_t size, int opcode, int reg, int imm);
int cg_x86_emit_reg16_imm8_instr(char *str, size_t size, int opcode, int reg, int imm);
int cg_x86_emit_reg32_imm32_instr(char *str, size_t size, int opcode, int reg, int imm);
int cg_x86_emit_reg32_imm8_instr(char *str, size_t size, int opcode, int reg, int imm);
int cg_x86_emit_reg8_imm8_instr(char *str, size_t size, int opcode, int reg, int imm);
int cg_x86_emit_reg16_reg16_instr(char *str, size_t size, int opcode, int rega, int regb);
int cg_x86_emit_reg32_reg32_instr(char *str, size_t size, int opcode, int rega, int regb);
int cg_x86_emit_reg8_reg8_instr(char *str, size_t size, int opcode, int rega, int regb);

char *cg_x86_get_last_error();
int cg_x86_get_mode();
void cg_x86_set_mode(int mode);
