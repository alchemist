/*
 * alchemist - code generation library
 * Copyright (C) 2008  Robbert Haarman
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include "alchemist/x86.h"

/** \file
 *
 * \section concepts Concepts
 *
 * This library is used to emit <em>instructions</em>.
 *
 * Instructions consist of an <em>opcode</em> and
 * zero, one, or two <em>arguments</em>.
 *
 * Arguments can be <em>immediate values</em>,
 * <em>registers</em>, or <em>memory locations</em>.
 *
 * An immediate value can be 8, 16, or 32 bits (immediate
 * values of these sizes are abbreviated <em>imm8</em>,
 * <em>imm16</em> and <em>imm32</em>, respectively).
 * Whichever the size of the immediate value, its value is
 * always given as an int in the API.
 *
 * Registers are specified using integers. The
 * CG_X86_REG_* macros can be used to designate specific
 * registers (the name of the register is used in place of
 * the *). In the emitted instructions, registers are
 * designated by their code, which can be obtained by
 * using the int used by this API as an index in the
 * array register_code.
 *
 * Memory addresses are specified using a base, a scale
 * factor, and index, and a displacement. Base and
 * index are registers, whereas scale factor and displacement
 * are integers. The scale factor must be 0, 1, 2, 4, or 8.
 * The actual address is given by
 * value_of_base_register + scale_factor * value_of_index_register
 * + displacement.
 */

/** Allocate memory for a value.
 * Allocates memory for a single value of a given type.
 * @param T The type of value to allocate memory for.
 * @return A pointer to the newly allocated memory, or NULL on error.
 */
#define NEW(T) (T*) my_malloc(sizeof(T))
#define BYTE char

#define INVALID_INSTRUCTION { error = CG_X86_ERR_INVALID_INSTRUCTION; return -1; }
#define INVALID_SCALE_FACTOR { error = CG_X86_ERR_INVALID_SCALE_FACTOR; return -1; }

#define EMIT_BYTE(X) { if(size > n) str[n] = (BYTE) (X); n++; }
#define EMIT_WORD(X) { EMIT_BYTE((X) & 0xff); EMIT_BYTE(((X) >> 8) & 0xff); }
#define EMIT_DWORD(X) { EMIT_BYTE((X) & 0xff); EMIT_BYTE(((X) >> 8) & 0xff); \
                         EMIT_BYTE(((X) >> 16) & 0xff); EMIT_BYTE(((X) >> 24) & 0xff); }
#define EMIT_MR(X, Y) { m = cg_x86_emit_mem_or_reg_arg(str + n, size - n, X, Y); \
                        if(m < 0) return m; else n += m; }
#define EMIT_REG_MODRM(REG, SPARE) { m = cg_x86_emit_reg_modrm(str + n, size - n, REG, SPARE); \
                                      if(m < 0) return m; else n+= m; }
#define EMIT_MEM_MODRM(BASE, SCALE, INDEX, DISPLACEMENT, SPARE) {\
  m = cg_x86_emit_mem_modrm(str + n, size - n, BASE, SCALE, INDEX, DISPLACEMENT, SPARE); \
  if(m < 0) return m; else n+= m; }

#define EMIT_JCC16(CC) cg_x86_emit_jcc16(str, size, CC, imm);
#define EMIT_JCC32(CC) cg_x86_emit_jcc32(str, size, CC, imm);
#define EMIT_OP_IMM16(OP) cg_x86_emit_op_imm16(str, size, OP, imm)
#define EMIT_OP_IMM16_IMM16(OP) cg_x86_emit_op_imm16_imm16(str, size, OP, imma, immb)
#define EMIT_OP_IMM16_IMM32(OP) cg_x86_emit_op_imm16_imm32(str, size, OP, imma, immb)
#define EMIT_OP_IMM32(OP) cg_x86_emit_op_imm32(str, size, OP, imm)
#define EMIT_OP_IMM8(OP) cg_x86_emit_op_imm8(str, size, OP, imm)
#define EMIT_OP_MEM_REG16(OP) cg_x86_emit_op_mem_reg16(str, size, OP, base, scale, index, displacement, reg)
#define EMIT_OP_MEM_REG32(OP) cg_x86_emit_op_mem_reg32(str, size, OP, base, scale, index, displacement, reg)
#define EMIT_OP_MEM_REG8(OP) cg_x86_emit_op_mem_reg8(str, size, OP, base, scale, index, displacement, reg)
#define EMIT_OP_REG_MEM16(OP) cg_x86_emit_op_mem_reg16(str, size, OP, base, scale, index, displacement, reg)
#define EMIT_OP_REG_MEM32(OP) cg_x86_emit_op_mem_reg32(str, size, OP, base, scale, index, displacement, reg)
#define EMIT_OP_REG_MEM8(OP) cg_x86_emit_op_mem_reg8(str, size, OP, base, scale, index, displacement, reg)
#define EMIT_OP_REG_REG16(OP) cg_x86_emit_op_reg_reg16(str, size, OP, rega, regb)
#define EMIT_OP_REG_REG32(OP) cg_x86_emit_op_reg_reg32(str, size, OP, rega, regb)
#define EMIT_OP_REG_REG8(OP) cg_x86_emit_op_reg_reg8(str, size, OP, rega, regb)
#define EMIT_OP_SPARE_MEM16(OP, SPARE) cg_x86_emit_op_spare_mem16(str, size, OP, SPARE, base, scale, index, displacement)
#define EMIT_OP_SPARE_MEM32(OP, SPARE) cg_x86_emit_op_spare_mem32(str, size, OP, SPARE, base, scale, index, displacement)
#define EMIT_OP_SPARE_MEM8(OP, SPARE) cg_x86_emit_op_spare_mem8(str, size, OP, SPARE, base, scale, index, displacement)
#define EMIT_OP_SPARE_MEM_IMM16(OP, SPARE) cg_x86_emit_op_spare_mem_imm16(str, size, OP, SPARE, base, scale, index, displacement, imm)
#define EMIT_OP_SPARE_MEM_IMM32(OP, SPARE) cg_x86_emit_op_spare_mem_imm32(str, size, OP, SPARE, base, scale, index, displacement, imm)
#define EMIT_OP_SPARE_MEM_IMM8(OP, SPARE) cg_x86_emit_op_spare_mem_imm8(str, size, OP, SPARE, base, scale, index, displacement, imm)
#define EMIT_OP_SPARE_REG16(OP, SPARE) cg_x86_emit_op_spare_reg16(str, size, OP, SPARE, reg)
#define EMIT_OP_SPARE_REG32(OP, SPARE) cg_x86_emit_op_spare_reg32(str, size, OP, SPARE, reg)
#define EMIT_OP_SPARE_REG8(OP, SPARE) cg_x86_emit_op_spare_reg8(str, size, OP, SPARE, reg)
#define EMIT_OP_SPARE_REG_IMM16(OP, SPARE) cg_x86_emit_op_spare_reg_imm16(str, size, OP, SPARE, reg, imm)
#define EMIT_OP_SPARE_REG_IMM32(OP, SPARE) cg_x86_emit_op_spare_reg_imm32(str, size, OP, SPARE, reg, imm)
#define EMIT_OP_SPARE_REG_IMM8(OP, SPARE) cg_x86_emit_op_spare_reg_imm8(str, size, OP, SPARE, reg, imm)

#define INSTR16 if(mode == CG_X86_MODE_32BIT) EMIT_BYTE('\x66')
#define INSTR32 if(mode == CG_X86_MODE_16BIT) EMIT_BYTE('\x66')
#define ADDRESS16 if(mode == CG_X86_MODE_32BIT) EMIT_BYTE('\x67')
#define ADDRESS32 if(mode == CG_X86_MODE_16BIT) EMIT_BYTE('\x67')

static char *error = NULL;
static int mode = CG_X86_MODE_32BIT;

/** Table translating CG_X86_OP_* macros to operation names. */
char *cg_x86_opcodes[] = {
  NULL,
  "add",
  "adc",
  "and",
  "call",
  "clc",
  "cld",
  "cli",
  "cmp",
  "dec",
  "div",
  "hlt",
  "idiv",
  "imul",
  "in",
  "inc",
  "insb",
  "insd",
  "insw",
  "int",
  "int1",
  "int3",
  "into",
  "iret",
  "iretd",
  "iretw",
  "ja",
  "jb",
  "jcxz",
  "jecxz",
  "jg",
  "jl",
  "jna",
  "jnb",
  "jng",
  "jnl",
  "jno",
  "jnp",
  "jns",
  "jnz",
  "jo",
  "jp",
  "js",
  "jz",
  "jmp",
  "lds",
  "les",
  "lfs",
  "lgs",
  "lss",
  "lea",
  "leave",
  "lgdt",
  "lidt",
  "lldt",
  "lmsw",
  "lodsb",
  "lodsd",
  "lodsw",
  "loop",
  "loopnz",
  "loopz",
  "lsl",
  "ltr",
  "mov",
  "movsb",
  "movsd",
  "movsw",
  "mul",
  "neg",
  "nop",
  "not",
  "or",
  "out",
  "pause",
  "pop",
  "popad",
  "popaw",
  "popfd",
  "popfw",
  "push",
  "pushad",
  "pushaw",
  "pushfd",
  "pushfw",
  "ret",
  "retf",
  "rol",
  "ror",
  "shl",
  "shr",
  "sub",
  "xchg",
  "xor",
};

/** Table translating CG_X86_REG_* macros to register names. */
char *cg_x86_registers[] = {
  NULL,
  "ax",
  "bx",
  "cx",
  "dx",
  "di",
  "si",
  "bp",
  "sp",
  "al",
  "bl",
  "cl",
  "dl",
  "ah",
  "bh",
  "ch",
  "dh",
  "eax",
  "ebx",
  "ecx",
  "edx",
  "edi",
  "esi",
  "ebp",
  "esp",
};

/** Table translating CG_X86_REG_* macros to register codes. */
static int register_code[] = {
  -1,
  0,
  3,
  1,
  2,
  7,
  6,
  5,
  4,
  0,
  3,
  1,
  2,
  4,
  7,
  5,
  6,
  0,
  3,
  1,
  2,
  7,
  6,
  5,
  4,
};

/** Get the last error. */
char *cg_x86_get_last_error() {
  return error;
}

/** Emits a modrm parameter that refers to a memory location.
 * @param str The buffer to store the modrm in.
 * @param size The size of the buffer.
 * @param base The register that holds the base of the memory location.
 * @param scale The scale factor (must be 0, 1, 2, 4, or 8).
 * @param index The register that holds the index (ignored if scale == 0).
 * @param displacement The displacement of the memory location.
 * @param spare The value to store in the spare bits of the modrm.
 *
 * @return The number of bytes needed to store the modrm.
 */
int cg_x86_emit_mem_modrm(char *str, size_t size, int base, int scale, int index, int displacement, int spare) {
  BYTE modrm, sib;
  int n = 0, address_mode = 0;

  modrm = spare << 3;

  /* Calculate the size of the displacement we need to use. */
  if(displacement == 0) {
    /* No displacement. */
    /* modrm |= 0; */
    modrm |= 0x40; /* Put one anyway... */
  } else if((displacement & 0xffffff00) == 0) {
    /* byte displacement. */
    modrm |= 0x40;
  } else if((displacement & 0xffff0000) == 0) {
    /* word displacement. */
    modrm |= 0x80;
    address_mode = 16;
  } else {
    /* dword displacement. */
    modrm |= 0x80;
    address_mode = 32;
  }

  /* Calculate SIB byte. */
  /* Scale must be 1, 2, 4, or 8 */
  switch(scale) {
  case 0:
  case 1:
    sib = 0x00;
    break;
  case 2:
    sib = 0x40;
    break;
  case 4:
    sib = 0x80;
    break;
  case 8:
    sib = 0xc0;
    break;
  default:
    /* No other values of scale are allowed. */
    INVALID_SCALE_FACTOR
  }

  /* Index */
  /* If scale == 0, we don't have an index. In that case,
   * the index code is 4 (so we or with 4 << 3, which is 0x20). */
  if(scale == 0) sib |= 0x20;
  else {
    /* ESP cannot be used as an index. */
    if(index == CG_X86_REG_ESP) INVALID_INSTRUCTION
    sib |= register_code[index] << 3;
  }

  /* Base */
  sib |= register_code[base];

  /* For now, we only support addressing using the SIB byte.
     That also means we must use 32-bit mode. */
  modrm |= 4;
  ADDRESS32;
  EMIT_BYTE(modrm);
  EMIT_BYTE(sib);
  switch(modrm & 0xc0) {
  case 0:
    break;
  case 0x40:
    EMIT_BYTE(displacement);
    break;
  case 0x80:
    EMIT_DWORD(displacement);
    break;
  default:
    /* Can't happen */
    return -1;
  }

  return n;
}

/** Emits a modrm that refers to a register.
 * @param str The buffer to store the emitted code in.
 * @param size The size of the buffer.
 * @param reg The register to encode in the modrm.
 * @param spare The value to encode in the spare of the modrm.
 *
 * @return The number of bytes needed to store the emitted modrm.
 */
int cg_x86_emit_reg_modrm(char *str, size_t size, int reg, int spare) {
  int n = 0;
  EMIT_BYTE(0xc0 | (spare << 3) | register_code[reg]);
  return n;
}

/** Emits a conditional jump instruction with a 16-bit offset.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param cc The condition code.
 * @param imm The offset.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_jcc16(char *str, size_t size, int cc, int imm) {
  int n = 0;
  INSTR16
  EMIT_BYTE(0x0f);
  EMIT_BYTE(0x80 + cc);
  EMIT_WORD(imm);
  return n;
}

/** Emits a conditional jump instruction with a 32-bit offset.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param cc The condition code.
 * @param imm The offset.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_jcc32(char *str, size_t size, int cc, int imm) {
  int n = 0;
  INSTR32
  EMIT_BYTE(0x0f);
  EMIT_BYTE(0x80 + cc);
  EMIT_DWORD(imm);
  return n;
}

/** Emits an instruction consisting of an opcode and a 16-bit immediate argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_imm16(char *str, size_t size, char op, int imm) {
  int n = 0;
  INSTR16
  EMIT_BYTE(op);
  EMIT_WORD(imm);
  return n;
}

/** Emits an instruction consisting of an opcode and two 16-bit immediate arguments.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param imma The first immediate value.
 * @param immb The second immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_imm16_imm16(char *str, size_t size, char op, int imma, int immb) {
  int n = 0;
  INSTR16
  EMIT_BYTE(op);
  EMIT_WORD(imma);
  EMIT_WORD(immb);
  return n;
}

/** Emits an instruction consisting of an opcode, one 16-bit immediate argument,
 * and one 32-bit immediate argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param imma The 16-bit immediate value.
 * @param immb The 32-bit immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_imm16_imm32(char *str, size_t size, char op, int imma, int immb) {
  int n = 0;
  INSTR32
  EMIT_BYTE(op);
  EMIT_WORD(imma);
  EMIT_DWORD(immb);
  return n;
}

/** Emits an instruction consisting of an opcode and a 32-bit immediate argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_imm32(char *str, size_t size, char op, int imm) {
  int n = 0;
  INSTR32
  EMIT_BYTE(op);
  EMIT_DWORD(imm);
  return n;
}

/** Emits an instruction consisting of an opcode and a 8-bit immediate argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_imm8(char *str, size_t size, char op, int imm) {
  int n = 0;
  EMIT_BYTE(op);
  EMIT_BYTE(imm);
  return n;
}

/** Emits an instruction consisting of an opcode and a modrm encoding a memory
 * address and a 16-bit register.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param base The base of the memory address.
 * @param scale The scale of the memory address.
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @param reg The register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_mem_reg16(char *str, size_t size, char op, int base, int scale, int index, int displacement, int reg) {
  int n = 0, m;
  INSTR16
  EMIT_BYTE(op);
  EMIT_MEM_MODRM(base, scale, index, displacement, register_code[reg]);
  return n;
}

/** Emits an instruction consisting of an opcode and a modrm encoding a memory
 * address and a 32-bit register.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param base The base of the memory address.
 * @param scale The scale of the memory address.
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @param reg The register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_mem_reg32(char *str, size_t size, char op, int base, int scale, int index, int displacement, int reg) {
  int n = 0, m;
  INSTR32
  EMIT_BYTE(op);
  EMIT_MEM_MODRM(base, scale, index, displacement, register_code[reg]);
  return n;
}

/** Emits an instruction consisting of an opcode and a modrm encoding a memory
 * address and an 8-bit register.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param base The base of the memory address.
 * @param scale The scale of the memory address.
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @param reg The register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_mem_reg8(char *str, size_t size, char op, int base, int scale, int index, int displacement, int reg) {
  int n = 0, m;
  EMIT_BYTE(op);
  EMIT_MEM_MODRM(base, scale, index, displacement, register_code[reg]);
  return n;
}

/** Emits an instruction consisting of an opcode and a modrm encoding two 16-bit registers.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param rega The first register.
 * @param regb The second register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_reg_reg16(char *str, size_t size, char op, int rega, int regb) {
  int n = 0, m;
  INSTR16
  EMIT_BYTE(op);
  EMIT_REG_MODRM(regb, register_code[rega]);
  return n;
}

/** Emits an instruction consisting of an opcode and a modrm encoding two 32-bit registers.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param rega The first register.
 * @param regb The second register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_reg_reg32(char *str, size_t size, char op, int rega, int regb) {
  int n = 0, m;
  INSTR32
  EMIT_BYTE(op);
  EMIT_REG_MODRM(regb, register_code[rega]);
  return n;
}

/** Emits an instruction consisting of an opcode and a modrm encoding two 8-bit registers.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param rega The first register.
 * @param regb The second register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_reg_reg8(char *str, size_t size, char op, int rega, int regb) {
  int n = 0, m;
  EMIT_BYTE(op);
  EMIT_REG_MODRM(regb, register_code[rega]);
  return n;
}

/** Emits an instruction consisting of an opcode, a spare, and a memory address
 * referring to a 16-bit value.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param spare The spare.
 * @param base The base of the memory address.
 * @param scale The scale of the memory address.
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_spare_mem16(char *str, size_t size, char op, int spare, int base, int scale, int index, int displacement) {
  int n = 0, m;
  INSTR16
  EMIT_BYTE(op);
  EMIT_MEM_MODRM(base, scale, index, displacement, spare);
  return n;
}

/** Emits an instruction consisting of an opcode, a spare, and a memory address
 * referring to a 32-bit value.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param spare The spare.
 * @param base The base of the memory address.
 * @param scale The scale of the memory address.
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_spare_mem32(char *str, size_t size, char op, int spare, int base, int scale, int index, int displacement) {
  int n = 0, m;
  INSTR32
  EMIT_BYTE(op);
  EMIT_MEM_MODRM(base, scale, index, displacement, spare);
  return n;
}

/** Emits an instruction consisting of an opcode, a spare, and a memory address
 * referring to a 8-bit value.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param spare The spare.
 * @param base The base of the memory address.
 * @param scale The scale of the memory address.
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_spare_mem8(char *str, size_t size, char op, int spare, int base, int scale, int index, int displacement) {
  int n = 0, m;
  EMIT_BYTE(op);
  EMIT_MEM_MODRM(base, scale, index, displacement, spare);
  return n;
}

/** Emits an instruction consisting of an opcode, a spare, and a 16-bit register.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param spare The spare.
 * @param reg The register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_spare_reg16(char *str, size_t size, char op, int spare, int reg) {
  int n = 0, m;
  INSTR16
  EMIT_BYTE(op);
  EMIT_REG_MODRM(reg, spare);
  return n;
}

/** Emits an instruction consisting of an opcode, a spare, and a 32-bit register.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param spare The spare.
 * @param reg The register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_spare_reg32(char *str, size_t size, char op, int spare, int reg) {
  int n = 0, m;
  INSTR32
  EMIT_BYTE(op);
  EMIT_REG_MODRM(reg, spare);
  return n;
}

/** Emits an instruction consisting of an opcode, a spare, and a 8-bit register.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param spare The spare.
 * @param reg The register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_spare_reg8(char *str, size_t size, char op, int spare, int reg) {
  int n = 0, m;
  EMIT_BYTE(op);
  EMIT_REG_MODRM(reg, spare);
  return n;
}

/** Emits an instruction consisting of an opcode, a spare, a register,
 * and a 16-bit immediate value.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param spare The spare.
 * @param reg The register.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_spare_reg_imm16(char *str, size_t size, char op, int spare, int reg, int imm) {
  int n = 0, m;
  INSTR16
  EMIT_BYTE(op);
  EMIT_REG_MODRM(reg, spare);
  EMIT_WORD(imm);
  return n;
}

/** Emits an instruction consisting of an opcode, a spare, a register,
 * and a 32-bit immediate value.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param spare The spare.
 * @param reg The register.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_spare_reg_imm32(char *str, size_t size, char op, int spare, int reg, int imm) {
  int n = 0, m;
  INSTR32
  EMIT_BYTE(op);
  EMIT_REG_MODRM(reg, spare);
  EMIT_DWORD(imm);
  return n;
}

/** Emits an instruction consisting of an opcode, a spare, a register,
 * and an 8-bit immediate value.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param spare The spare.
 * @param reg The register.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_spare_reg_imm8(char *str, size_t size, char op, int spare, int reg, int imm) {
  int n = 0, m;
  EMIT_BYTE(op);
  EMIT_REG_MODRM(reg, spare);
  EMIT_BYTE(imm);
  return n;
}

/** Emits an instruction consiting of an opcode, a spare, a memory address,
 * and a 16-bit immediate value.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param spare The spare.
 * @param base The base of the memory address.
 * @param scale The scale of the memory address.
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_spare_mem_imm16(char *str, size_t size, char op, int spare, int base, int scale, int index, int displacement, int imm) {
  int n = 0, m;
  INSTR16
  EMIT_BYTE(op);
  EMIT_MEM_MODRM(base, scale, index, displacement, spare);
  EMIT_WORD(imm);
  return n;
}

/** Emits an instruction consiting of an opcode, a spare, a memory address,
 * and a 32-bit immediate value.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param spare The spare.
 * @param base The base of the memory address.
 * @param scale The scale of the memory address.
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_spare_mem_imm32(char *str, size_t size, char op, int spare, int base, int scale, int index, int displacement, int imm) {
  int n = 0, m;
  INSTR32
  EMIT_BYTE(op);
  EMIT_MEM_MODRM(base, scale, index, displacement, spare);
  EMIT_DWORD(imm);
  return n;
}

/** Emits an instruction consiting of an opcode, a spare, a memory address,
 * and a 8-bit immediate value.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param op The opcode.
 * @param spare The spare.
 * @param base The base of the memory address.
 * @param scale The scale of the memory address.
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_op_spare_mem_imm8(char *str, size_t size, char op, int spare, int base, int scale, int index, int displacement, int imm) {
  int n = 0, m;
  EMIT_BYTE(op);
  EMIT_MEM_MODRM(base, scale, index, displacement, spare);
  EMIT_BYTE(imm);
  return n;
}

/** Emits an instruction without any arguments.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_nullary_instr(char *str, size_t size, int opcode) {
  int n = 0;
  switch(opcode) {
  case CG_X86_OP_CLC:
    EMIT_BYTE(0xf8);
    break;
  case CG_X86_OP_CLD:
    EMIT_BYTE(0xfc);
    break;
  case CG_X86_OP_CLI:
    EMIT_BYTE(0xfa);
    break;
  case CG_X86_OP_RET:
    EMIT_BYTE(0xc3);
    break;
  case CG_X86_OP_RETF:
    EMIT_BYTE(0xcb);
    break;
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a 16-bit immediate argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_imm16_instr(char *str, size_t size, int opcode, int imm) {
  int n = 0;
  switch(opcode) {
  case CG_X86_OP_CALL:
    return EMIT_OP_IMM16(0xe8);
  case CG_X86_OP_JA:
    return EMIT_JCC16(7);
  case CG_X86_OP_JB:
    return EMIT_JCC16(2);
  case CG_X86_OP_JG:
    return EMIT_JCC16(15);
  case CG_X86_OP_JL:
    return EMIT_JCC16(12);
  case CG_X86_OP_JNA:
    return EMIT_JCC16(6);
  case CG_X86_OP_JNB:
    return EMIT_JCC16(3);
  case CG_X86_OP_JNG:
    return EMIT_JCC16(14);
  case CG_X86_OP_JNL:
    return EMIT_JCC16(13);
  case CG_X86_OP_JNO:
    return EMIT_JCC16(1);
  case CG_X86_OP_JNP:
    return EMIT_JCC16(11);
  case CG_X86_OP_JNS:
    return EMIT_JCC16(9);
  case CG_X86_OP_JNZ:
    return EMIT_JCC16(5);
  case CG_X86_OP_JO:
    return EMIT_JCC16(0);
  case CG_X86_OP_JP:
    return EMIT_JCC16(10);
  case CG_X86_OP_JS:
    return EMIT_JCC16(8);
  case CG_X86_OP_JZ:
    return EMIT_JCC16(4);
  case CG_X86_OP_JMP:
    return EMIT_OP_IMM16(0xe9);
  case CG_X86_OP_RET:
    return EMIT_OP_IMM16(0xc2);
  case CG_X86_OP_RETF:
    return EMIT_OP_IMM16(0xca);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with two 16-bit immediate arguments.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param imma The first immediate value.
 * @param immb The second immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_imm16_imm16_instr(char *str, size_t size, int opcode, int imma, int immb) {
  int n = 0;
  switch(opcode) {
  case CG_X86_OP_CALL:
    return EMIT_OP_IMM16_IMM16(0x9a);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with one 16-bit immediate argument and one 32-bit
 * immediate argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param imma The 16-bit immediate value.
 * @param immb The 32-bit immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_imm16_imm32_instr(char *str, size_t size, int opcode, int imma, int immb) {
  int n = 0;
  switch(opcode) {
  case CG_X86_OP_CALL:
    return EMIT_OP_IMM16_IMM32(0x9a);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a 32-bit immediate argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_imm32_instr(char *str, size_t size, int opcode, int imm) {
  int n = 0;
  switch(opcode) {
  case CG_X86_OP_CALL:
    return EMIT_OP_IMM32(0xe8);
  case CG_X86_OP_JA:
    return EMIT_JCC32(7);
  case CG_X86_OP_JB:
    return EMIT_JCC32(2);
  case CG_X86_OP_JG:
    return EMIT_JCC32(15);
  case CG_X86_OP_JL:
    return EMIT_JCC32(12);
  case CG_X86_OP_JNA:
    return EMIT_JCC32(6);
  case CG_X86_OP_JNB:
    return EMIT_JCC32(3);
  case CG_X86_OP_JNG:
    return EMIT_JCC32(14);
  case CG_X86_OP_JNL:
    return EMIT_JCC32(13);
  case CG_X86_OP_JNO:
    return EMIT_JCC32(1);
  case CG_X86_OP_JNP:
    return EMIT_JCC32(11);
  case CG_X86_OP_JNS:
    return EMIT_JCC32(9);
  case CG_X86_OP_JNZ:
    return EMIT_JCC32(5);
  case CG_X86_OP_JO:
    return EMIT_JCC32(0);
  case CG_X86_OP_JP:
    return EMIT_JCC32(10);
  case CG_X86_OP_JS:
    return EMIT_JCC32(8);
  case CG_X86_OP_JZ:
    return EMIT_JCC32(4);
  case CG_X86_OP_JMP:
    return EMIT_OP_IMM32(0xe9);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with an 8-bit immediate argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_imm8_instr(char *str, size_t size, int opcode, int imm) {
  int n = 0;
  switch(opcode) {
  case CG_X86_OP_INT:
    return EMIT_OP_IMM8(0xcd);
  case CG_X86_OP_JA:
    return EMIT_OP_IMM8(0x77);
  case CG_X86_OP_JB:
    return EMIT_OP_IMM8(0x72);
  case CG_X86_OP_JCXZ:
    return EMIT_OP_IMM8(0xe3);
  case CG_X86_OP_JECXZ:
    return EMIT_OP_IMM8(0xe3);
  case CG_X86_OP_JG:
    return EMIT_OP_IMM8(0x85);
  case CG_X86_OP_JL:
    return EMIT_OP_IMM8(0x82);
  case CG_X86_OP_JNA:
    return EMIT_OP_IMM8(0x76);
  case CG_X86_OP_JNB:
    return EMIT_OP_IMM8(0x73);
  case CG_X86_OP_JNG:
    return EMIT_OP_IMM8(0x84);
  case CG_X86_OP_JNL:
    return EMIT_OP_IMM8(0x83);
  case CG_X86_OP_JNO:
    return EMIT_OP_IMM8(0x71);
  case CG_X86_OP_JNP:
    return EMIT_OP_IMM8(0x81);
  case CG_X86_OP_JNS:
    return EMIT_OP_IMM8(0x79);
  case CG_X86_OP_JNZ:
    return EMIT_OP_IMM8(0x75);
  case CG_X86_OP_JO:
    return EMIT_OP_IMM8(0x70);
  case CG_X86_OP_JP:
    return EMIT_OP_IMM8(0x80);
  case CG_X86_OP_JS:
    return EMIT_OP_IMM8(0x78);
  case CG_X86_OP_JZ:
    return EMIT_OP_IMM8(0x74);
  case CG_X86_OP_JMP:
    return EMIT_OP_IMM8(0xeb);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a memory address that refers to a 16-bit value.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param base The base of the memory address.
 * @param scale The scale factor of the memory address (must be 0, 1, 2, 4, or 8).
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_mem16_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement) {
  int n = 0;
  switch(opcode) {
  case CG_X86_OP_CALL:
    return EMIT_OP_SPARE_MEM16(0xff, 2);
  case CG_X86_OP_INC:
    return EMIT_OP_SPARE_MEM16(0xff, 0);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a memory address that refers to a 16-bit value
 * as its first argument, and a 16-bit immediate value as its second argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param base The base of the memory address.
 * @param scale The scale factor of the memory address (must be 0, 1, 2, 4, or 8).
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_mem16_imm16_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement, int imm) {
  int n = 0, m, spare;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_SPARE_MEM_IMM16(0x81, 2);
  case CG_X86_OP_ADD:
    return EMIT_OP_SPARE_MEM_IMM16(0x81, 0);
  case CG_X86_OP_AND:
    return EMIT_OP_SPARE_MEM_IMM16(0x81, 4);
  case CG_X86_OP_CMP:
    return EMIT_OP_SPARE_MEM_IMM16(0x81, 7);
  case CG_X86_OP_OR:
    return EMIT_OP_SPARE_MEM_IMM16(0x81, 1);
  case CG_X86_OP_MOV:
    return EMIT_OP_SPARE_MEM_IMM16(0xc7, 0);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a memory address that refers to a 16-bit value
 * as its first argument, and a 16-bit register as its second argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param base The base of the memory address.
 * @param scale The scale factor of the memory address (must be 0, 1, 2, 4, or 8).
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @param reg The register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_mem16_reg16_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement, int reg) {
  int n = 0, m;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_MEM_REG16(0x11);
  case CG_X86_OP_ADD:
    return EMIT_OP_MEM_REG16(0x01);
  case CG_X86_OP_AND:
    return EMIT_OP_MEM_REG16(0x23);
  case CG_X86_OP_CMP:
    return EMIT_OP_MEM_REG16(0x39);
  case CG_X86_OP_OR:
    return EMIT_OP_MEM_REG16(0x09);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a memory address that refers to a 32-bit value.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param base The base of the memory address.
 * @param scale The scale factor of the memory address (must be 0, 1, 2, 4, or 8).
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_mem32_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement) {
  int n = 0;
  switch(opcode) {
  case CG_X86_OP_CALL:
    return EMIT_OP_SPARE_MEM32(0xff, 2);
  case CG_X86_OP_INC:
    return EMIT_OP_SPARE_MEM32(0xff, 0);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a memory address that refers to a 32-bit value
 * as its first argument, and a 32-bit immediate value as its second argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param base The base of the memory address.
 * @param scale The scale factor of the memory address (must be 0, 1, 2, 4, or 8).
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_mem32_imm32_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement, int imm) {
  int n = 0, m, spare;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_SPARE_MEM_IMM32(0x81, 2);
  case CG_X86_OP_ADD:
    return EMIT_OP_SPARE_MEM_IMM32(0x81, 0);
  case CG_X86_OP_AND:
    return EMIT_OP_SPARE_MEM_IMM32(0x81, 4);
  case CG_X86_OP_CMP:
    return EMIT_OP_SPARE_MEM_IMM32(0x81, 7);
  case CG_X86_OP_OR:
    return EMIT_OP_SPARE_MEM_IMM32(0x81, 1);
  case CG_X86_OP_MOV:
    return EMIT_OP_SPARE_MEM_IMM32(0xc7, 0);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a memory address that refers to a 32-bit value
 * as its first argument, and a 32-bit register as its second argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param base The base of the memory address.
 * @param scale The scale factor of the memory address (must be 0, 1, 2, 4, or 8).
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @param reg The register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_mem32_reg32_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement, int reg) {
  int n = 0, m;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_MEM_REG32(0x11);
  case CG_X86_OP_ADD:
    return EMIT_OP_MEM_REG32(0x01);
  case CG_X86_OP_AND:
    return EMIT_OP_MEM_REG32(0x23);
  case CG_X86_OP_CMP:
    return EMIT_OP_MEM_REG32(0x39);
  case CG_X86_OP_OR:
    return EMIT_OP_MEM_REG32(0x09);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a memory address that refers to an 8-bit value.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param base The base of the memory address.
 * @param scale The scale factor of the memory address (must be 0, 1, 2, 4, or 8).
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_mem8_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement) {
  int n = 0;
  switch(opcode) {
  case CG_X86_OP_INC:
    return EMIT_OP_SPARE_MEM8(0xfe, 0);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a memory address that refers to an 8-bit value
 * as its first argument, and a 8-bit immediate value as its second argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param base The base of the memory address.
 * @param scale The scale factor of the memory address (must be 0, 1, 2, 4, or 8).
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_mem8_imm8_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement, int imm) {
  int n = 0, m;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_SPARE_MEM_IMM8(0x80, 2);
  case CG_X86_OP_ADD:
    return EMIT_OP_SPARE_MEM_IMM8(0x80, 0);
  case CG_X86_OP_AND:
    return EMIT_OP_SPARE_MEM_IMM8(0x80, 4);
  case CG_X86_OP_CMP:
    return EMIT_OP_SPARE_MEM_IMM8(0x80, 7);
  case CG_X86_OP_OR:
    return EMIT_OP_SPARE_MEM_IMM8(0x80, 1);
  case CG_X86_OP_MOV:
    return EMIT_OP_SPARE_MEM_IMM8(0xc6, 0);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a memory address that refers to an 8-bit value
 * as its first argument, and a 8-bit register as its second argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param base The base of the memory address.
 * @param scale The scale factor of the memory address (must be 0, 1, 2, 4, or 8).
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @param reg The register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_mem8_reg8_instr(char *str, size_t size, int opcode, int base, int scale, int index, int displacement, int reg) {
  int n = 0, m;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_MEM_REG8(0x10);
  case CG_X86_OP_ADD:
    return EMIT_OP_MEM_REG8(0x00);
  case CG_X86_OP_AND:
    return EMIT_OP_MEM_REG8(0x20);
  case CG_X86_OP_CMP:
    return EMIT_OP_MEM_REG8(0x38);
  case CG_X86_OP_OR:
    return EMIT_OP_MEM_REG8(0x08);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a 16-bit register argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param reg The register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg16_instr(char *str, size_t size, int opcode, int reg) {
  int n = 0;
  switch(opcode) {
  case CG_X86_OP_CALL:
    return EMIT_OP_SPARE_REG16(0xff, 2);
  case CG_X86_OP_DEC:
    INSTR16
    EMIT_BYTE(0x48 + register_code[reg]);
    break;
  case CG_X86_OP_DIV:
    return EMIT_OP_SPARE_REG16(0xf7, 6);
  case CG_X86_OP_IDIV:
    return EMIT_OP_SPARE_REG16(0xf7, 7);
  case CG_X86_OP_IMUL:
    return EMIT_OP_SPARE_REG16(0xf7, 5);
  case CG_X86_OP_INC:
    INSTR16
    EMIT_BYTE(0x40 + register_code[reg]);
    break;
  case CG_X86_OP_MUL:
    return EMIT_OP_SPARE_REG16(0xf7, 4);
  case CG_X86_OP_NEG:
    return EMIT_OP_SPARE_REG16(0xf7, 3);
  case CG_X86_OP_NOT:
    return EMIT_OP_SPARE_REG16(0xf7, 2);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a 16-bit register argument and
 * a 16-bit immediate argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param reg The register.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg16_imm16_instr(char *str, size_t size, int opcode, int reg, int imm) {
  int n = 0, m, spare;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    if(reg == CG_X86_REG_AX) {
      return EMIT_OP_IMM16(0x15);
    } else {
      return EMIT_OP_SPARE_REG_IMM16(0x81, 2);
    }
  case CG_X86_OP_ADD:
    if(reg == CG_X86_REG_AX) {
      return EMIT_OP_IMM16(0x05);
    } else {
      return EMIT_OP_SPARE_REG_IMM16(0x81, 0);
    }
  case CG_X86_OP_AND:
    if(reg == CG_X86_REG_AX) {
      return EMIT_OP_IMM16(0x25);
    } else {
      return EMIT_OP_SPARE_REG_IMM16(0x81, 4);
    }
  case CG_X86_OP_CMP:
    if(reg == CG_X86_REG_AX) {
      return EMIT_OP_IMM16(0x3d);
    } else {
      return EMIT_OP_SPARE_REG_IMM16(0x81, 7);
    }
  case CG_X86_OP_OR:
    if(reg == CG_X86_REG_AX) {
      return EMIT_OP_IMM16(0x0d);
    } else {
      return EMIT_OP_SPARE_REG_IMM16(0x81, 1);
    }
  case CG_X86_OP_XOR:
    if(reg == CG_X86_REG_AX) {
      return EMIT_OP_IMM16(0x35);
    } else {
      return EMIT_OP_SPARE_REG_IMM16(0x81, 6);
    }
  case CG_X86_OP_MOV:
    return EMIT_OP_IMM16(0xb8 + register_code[reg]);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a 16-bit register argument and
 * an 8-bit immediate argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param reg The register.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg16_imm8_instr(char *str, size_t size, int opcode, int reg, int imm) {
  int n = 0, m, spare;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_SPARE_REG_IMM8(0x83, 2);
  case CG_X86_OP_ADD:
    return EMIT_OP_SPARE_REG_IMM8(0x83, 0);
  case CG_X86_OP_AND:
    return EMIT_OP_SPARE_REG_IMM8(0x83, 4);
  case CG_X86_OP_CMP:
    return EMIT_OP_SPARE_REG_IMM8(0x83, 7);
  case CG_X86_OP_OR:
    return EMIT_OP_SPARE_REG_IMM8(0x83, 1);
  case CG_X86_OP_XOR:
    return EMIT_OP_SPARE_REG_IMM8(0x83, 6);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a 16-bit register as its first argument
 * and a memory address that refers to a 16-bit value as its second
 * argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param reg The register.
 * @param base The base of the memory address.
 * @param scale The scale factor of the memory address (must be 0, 1, 2, 4, or 8).
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg16_mem16_instr(char *str, size_t size, int opcode, int reg, int base, int scale, int index, int displacement) {
  int n = 0, m;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_REG_MEM16(0x13);
  case CG_X86_OP_ADD:
    return EMIT_OP_REG_MEM16(0x03);
  case CG_X86_OP_AND:
    return EMIT_OP_REG_MEM16(0x23);
  case CG_X86_OP_CMP:
    return EMIT_OP_REG_MEM16(0x3b);
  case CG_X86_OP_OR:
    return EMIT_OP_REG_MEM16(0x0b);
  case CG_X86_OP_XOR:
    return EMIT_OP_REG_MEM16(0x33);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with two 16-bit register arguments.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param rega The first register.
 * @param regb The second register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg16_reg16_instr(char *str, size_t size, int opcode, int rega, int regb) {
  int n = 0, m;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_REG_REG16(0x13);
  case CG_X86_OP_ADD:
    return EMIT_OP_REG_REG16(0x03);
  case CG_X86_OP_AND:
    return EMIT_OP_REG_REG16(0x23);
  case CG_X86_OP_CMP:
    return EMIT_OP_REG_REG16(0x3b);
  case CG_X86_OP_OR:
    return EMIT_OP_REG_REG16(0x0b);
  case CG_X86_OP_XOR:
    return EMIT_OP_REG_REG16(0x33);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a 32-bit register argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param reg The register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg32_instr(char *str, size_t size, int opcode, int reg) {
  int n = 0;
  switch(opcode) {
  case CG_X86_OP_CALL:
    return EMIT_OP_SPARE_REG32(0xff, 2);
  case CG_X86_OP_DEC:
    INSTR32
    EMIT_BYTE(0x48 + register_code[reg]);
    break;
  case CG_X86_OP_DIV:
    return EMIT_OP_SPARE_REG32(0xf7, 6);
  case CG_X86_OP_IDIV:
    return EMIT_OP_SPARE_REG32(0xf7, 7);
  case CG_X86_OP_IMUL:
    return EMIT_OP_SPARE_REG32(0xf7, 5);
  case CG_X86_OP_INC:
    INSTR32
    EMIT_BYTE(0x40 + register_code[reg]);
    break;
  case CG_X86_OP_MUL:
    return EMIT_OP_SPARE_REG32(0xf7, 4);
  case CG_X86_OP_NEG:
    return EMIT_OP_SPARE_REG32(0xf7, 3);
  case CG_X86_OP_NOT:
    return EMIT_OP_SPARE_REG32(0xf7, 2);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a 32-bit register argument and
 * a 32-bit immediate argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param reg The register.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg32_imm32_instr(char *str, size_t size, int opcode, int reg, int imm) {
  int n = 0, m, spare;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    if(reg == CG_X86_REG_EAX) {
      return EMIT_OP_IMM32(0x15);
    } else {
      return EMIT_OP_SPARE_REG_IMM32(0x81, 2);
    }
  case CG_X86_OP_ADD:
    if(reg == CG_X86_REG_EAX) {
      return EMIT_OP_IMM32(0x05);
    } else {
      return EMIT_OP_SPARE_REG_IMM32(0x81, 0);
    }
  case CG_X86_OP_AND:
    if(reg == CG_X86_REG_EAX) {
      return EMIT_OP_IMM32(0x25);
    } else {
      return EMIT_OP_SPARE_REG_IMM32(0x81, 4);
    }
  case CG_X86_OP_CMP:
    if(reg == CG_X86_REG_EAX) {
      return EMIT_OP_IMM32(0x3d);
    } else {
      return EMIT_OP_SPARE_REG_IMM32(0x81, 7);
    }
  case CG_X86_OP_OR:
    if(reg == CG_X86_REG_EAX) {
      return EMIT_OP_IMM32(0x0d);
    } else {
      return EMIT_OP_SPARE_REG_IMM32(0x81, 1);
    }
  case CG_X86_OP_XOR:
    if(reg == CG_X86_REG_EAX) {
      return EMIT_OP_IMM32(0x35);
    } else {
      return EMIT_OP_SPARE_REG_IMM32(0x81, 6);
    }
  case CG_X86_OP_MOV:
    return EMIT_OP_IMM32(0xb8 + register_code[reg]);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a 32-bit register argument and
 * an 8-bit immediate argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param reg The register.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg32_imm8_instr(char *str, size_t size, int opcode, int reg, int imm) {
  int n = 0, m, spare;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_SPARE_REG_IMM8(0x83, 2);
  case CG_X86_OP_ADD:
    return EMIT_OP_SPARE_REG_IMM8(0x83, 0);
  case CG_X86_OP_AND:
    return EMIT_OP_SPARE_REG_IMM8(0x83, 4);
  case CG_X86_OP_CMP:
    return EMIT_OP_SPARE_REG_IMM8(0x83, 7);
  case CG_X86_OP_OR:
    return EMIT_OP_SPARE_REG_IMM8(0x83, 1);
  case CG_X86_OP_XOR:
    return EMIT_OP_SPARE_REG_IMM8(0x83, 6);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with a 32-bit register as its first argument
 * and a memory address that refers to a 32-bit value as its second
 * argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param reg The register.
 * @param base The base of the memory address.
 * @param scale The scale factor of the memory address (must be 0, 1, 2, 4, or 8).
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg32_mem32_instr(char *str, size_t size, int opcode, int reg, int base, int scale, int index, int displacement) {
  int n = 0, m;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_REG_MEM32(0x13);
  case CG_X86_OP_ADD:
    return EMIT_OP_REG_MEM32(0x03);
  case CG_X86_OP_AND:
    return EMIT_OP_REG_MEM32(0x23);
  case CG_X86_OP_CMP:
    return EMIT_OP_REG_MEM32(0x3b);
  case CG_X86_OP_OR:
    return EMIT_OP_REG_MEM32(0x0b);
  case CG_X86_OP_XOR:
    return EMIT_OP_REG_MEM32(0x33);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with two 32-bit register arguments.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param rega The first register.
 * @param regb The second register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg32_reg32_instr(char *str, size_t size, int opcode, int rega, int regb) {
  int n = 0, m;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_REG_REG32(0x13);
  case CG_X86_OP_ADD:
    return EMIT_OP_REG_REG32(0x03);
  case CG_X86_OP_AND:
    return EMIT_OP_REG_REG32(0x23);
  case CG_X86_OP_CMP:
    return EMIT_OP_REG_REG32(0x3b);
  case CG_X86_OP_OR:
    return EMIT_OP_REG_REG32(0x0b);
  case CG_X86_OP_XOR:
    return EMIT_OP_REG_REG32(0x33);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with an 8-bit register argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param reg The register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg8_instr(char *str, size_t size, int opcode, int reg) {
  int n = 0;
  switch(opcode) {
  case CG_X86_OP_DEC:
    return EMIT_OP_SPARE_REG8(0xfe, 1);
  case CG_X86_OP_DIV:
    return EMIT_OP_SPARE_REG8(0xf6, 6);
  case CG_X86_OP_IDIV:
    return EMIT_OP_SPARE_REG8(0xf6, 7);
  case CG_X86_OP_IMUL:
    return EMIT_OP_SPARE_REG8(0xf6, 5);
  case CG_X86_OP_INC:
    return EMIT_OP_SPARE_REG8(0xfe, 0);
  case CG_X86_OP_MUL:
    return EMIT_OP_SPARE_REG8(0xf6, 4);
  case CG_X86_OP_NEG:
    return EMIT_OP_SPARE_REG8(0xf6, 3);
  case CG_X86_OP_NOT:
    return EMIT_OP_SPARE_REG8(0xf6, 2);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with an 8-bit register argument and
 * an 8-bit immediate argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param reg The register.
 * @param imm The immediate value.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg8_imm8_instr(char *str, size_t size, int opcode, int reg, int imm) {
  int n = 0, m, spare;
  char op;
  switch(opcode) {
  case CG_X86_OP_ADC:
    if(reg == CG_X86_REG_AL) {
      return EMIT_OP_IMM8(0x14);
    } else {
      return EMIT_OP_SPARE_REG_IMM8(0x80, 2);
    }
  case CG_X86_OP_ADD:
    if(reg == CG_X86_REG_AL) {
      return EMIT_OP_IMM8(0x04);
    } else {
      return EMIT_OP_SPARE_REG_IMM8(0x80, 0);
    }
  case CG_X86_OP_AND:
    if(reg == CG_X86_REG_AL) {
      return EMIT_OP_IMM8(0x24);
    } else {
      return EMIT_OP_SPARE_REG_IMM8(0x80, 4);
    }
  case CG_X86_OP_CMP:
    if(reg == CG_X86_REG_AL) {
      return EMIT_OP_IMM8(0x3c);
    } else {
      return EMIT_OP_SPARE_REG_IMM8(0x80, 7);
    }
  case CG_X86_OP_OR:
    if(reg == CG_X86_REG_AL) {
      return EMIT_OP_IMM8(0x0c);
    } else {
      return EMIT_OP_SPARE_REG_IMM8(0x80, 1);
    }
  case CG_X86_OP_XOR:
    if(reg == CG_X86_REG_AL) {
      return EMIT_OP_IMM8(0x34);
    } else {
      return EMIT_OP_SPARE_REG_IMM8(0x80, 6);
    }
  case CG_X86_OP_MOV:
    return EMIT_OP_IMM8(0xb0 + register_code[reg]);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with an 8-bit register as its first argument
 * and a memory address that refers to an 8-bit value as its second
 * argument.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param reg The register.
 * @param base The base of the memory address.
 * @param scale The scale factor of the memory address (must be 0, 1, 2, 4, or 8).
 * @param index The index of the memory address.
 * @param displacement The displacement of the memory address.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg8_mem8_instr(char *str, size_t size, int opcode, int reg, int base, int scale, int index, int displacement) {
  int n = 0, m;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_MEM_REG8(0x12);
  case CG_X86_OP_ADD:
    return EMIT_OP_MEM_REG8(0x02);
  case CG_X86_OP_AND:
    return EMIT_OP_MEM_REG8(0x22);
  case CG_X86_OP_CMP:
    return EMIT_OP_MEM_REG8(0x3a);
  case CG_X86_OP_OR:
    return EMIT_OP_MEM_REG8(0x0a);
  case CG_X86_OP_XOR:
    return EMIT_OP_MEM_REG8(0x32);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}

/** Emits an instruction with two 8-bit register arguments.
 * @param str The buffer to emit the instruction to.
 * @param size The size of the buffer.
 * @param opcode The operation the instruction performs.
 * @param rega The first register.
 * @param regb The second register.
 * @return The number of bytes needed to store the instruction.
 */
int cg_x86_emit_reg8_reg8_instr(char *str, size_t size, int opcode, int rega, int regb) {
  int n = 0, m;
  switch(opcode) {
  case CG_X86_OP_ADC:
    return EMIT_OP_REG_REG8(0x12);
  case CG_X86_OP_ADD:
    return EMIT_OP_REG_REG8(0x02);
  case CG_X86_OP_AND:
    return EMIT_OP_REG_REG8(0x22);
  case CG_X86_OP_CMP:
    return EMIT_OP_REG_REG8(0x3a);
  case CG_X86_OP_OR:
    return EMIT_OP_REG_REG8(0x0a);
  case CG_X86_OP_XOR:
    return EMIT_OP_REG_REG8(0x32);
  default:
    INVALID_INSTRUCTION
  }
  return n;
}
