#include "alchemist/x86.h"

int main(int argc, char **argv) {
  char code[0x10000];
  int n = 0;

  /*** write code ***/

  /* xor eax, eax */
  n += cg_x86_emit_reg32_reg32_instr(code + n,
				     sizeof(code - n),
				     CG_X86_OP_XOR,
				     CG_X86_REG_EAX,
				     CG_X86_REG_EAX);
  /* inc eax */
  n += cg_x86_emit_reg32_instr(code + n,
			       sizeof(code) - n,
			       CG_X86_OP_INC,
			       CG_X86_REG_EAX);
  /* xor ebx, ebx */
  n += cg_x86_emit_reg32_reg32_instr(code + n,
				     sizeof(code) - n,
				     CG_X86_OP_XOR,
				     CG_X86_REG_EBX,
				     CG_X86_REG_EBX);
  /* or ebx, 42 */
  n += cg_x86_emit_reg32_imm8_instr(code + n,
				    sizeof(code) - n,
				    CG_X86_OP_OR,
				    CG_X86_REG_EBX,
				    42);
  /* int 0x80 */
  n += cg_x86_emit_imm8_instr(code + n,
			      sizeof(code) - n,
			      CG_X86_OP_INT,
			      0x80);

  /*** invoke code ***/
  ((void (*) ()) code)();

  /*** should not be reached ***/
  return 0;
}
