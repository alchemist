#include <stdio.h>
#include <alloca.h>
#include "x86.h"

#define AL CG_X86_REG_AL
#define AH CG_X86_REG_AH
#define BL CG_X86_REG_BL
#define BH CG_X86_REG_BH
#define CL CG_X86_REG_CL
#define CH CG_X86_REG_CH
#define DL CG_X86_REG_DL
#define DH CG_X86_REG_DH
#define AX CG_X86_REG_AX
#define BX CG_X86_REG_BX
#define CX CG_X86_REG_CX
#define DX CG_X86_REG_DX
#define SI CG_X86_REG_SI
#define DI CG_X86_REG_DI
#define BP CG_X86_REG_BP
#define SP CG_X86_REG_SP
#define EAX CG_X86_REG_EAX
#define EBX CG_X86_REG_EBX
#define ECX CG_X86_REG_ECX
#define EDX CG_X86_REG_EDX
#define ESI CG_X86_REG_ESI
#define EDI CG_X86_REG_EDI
#define EBP CG_X86_REG_EBP
#define ESP CG_X86_REG_ESP

#define ADC CG_X86_OP_ADC
#define ADD CG_X86_OP_ADD
#define INC CG_X86_OP_INC
#define INT CG_X86_OP_INT
#define OR CG_X86_OP_OR
#define XOR CG_X86_OP_XOR

#define EMIT write_code(stdout, buffer, n)
#define EMIT_IMM8(OPCODE, IMM) \
  n = cg_x86_emit_imm8_instr(buffer, sizeof(buffer), OPCODE, IMM); EMIT
#define EMIT_MEM8_REG8(OPCODE, BASE, SCALE, INDEX, DISPLACEMENT, REG) \
  n = cg_x86_emit_mem8_reg8_instr(buffer, sizeof(buffer), OPCODE, BASE, SCALE, INDEX, DISPLACEMENT, REG); EMIT
#define EMIT_MEM16_REG16(OPCODE, BASE, SCALE, INDEX, DISPLACEMENT, REG) \
  n = cg_x86_emit_mem16_reg16_instr(buffer, sizeof(buffer), OPCODE, BASE, SCALE, INDEX, DISPLACEMENT, REG); EMIT
#define EMIT_MEM32_REG32(OPCODE, BASE, SCALE, INDEX, DISPLACEMENT, REG) \
  n = cg_x86_emit_mem32_reg32_instr(buffer, sizeof(buffer), OPCODE, BASE, SCALE, INDEX, DISPLACEMENT, REG); EMIT
#define EMIT_MEM8_IMM8(OPCODE, BASE, SCALE, INDEX, DISPLACEMENT, IMM) \
  n = cg_x86_emit_mem8_imm8_instr(buffer, sizeof(buffer), OPCODE, BASE, SCALE, INDEX, DISPLACEMENT, IMM); EMIT
#define EMIT_MEM16_IMM16(OPCODE, BASE, SCALE, INDEX, DISPLACEMENT, IMM) \
  n = cg_x86_emit_mem16_imm16_instr(buffer, sizeof(buffer), OPCODE, BASE, SCALE, INDEX, DISPLACEMENT, IMM); EMIT
#define EMIT_MEM32_IMM32(OPCODE, BASE, SCALE, INDEX, DISPLACEMENT, IMM) \
  n = cg_x86_emit_mem32_imm32_instr(buffer, sizeof(buffer), OPCODE, BASE, SCALE, INDEX, DISPLACEMENT, IMM); EMIT
#define EMIT_REG8_IMM8(OPCODE, REG, IMM) \
  n = cg_x86_emit_reg8_imm8_instr(buffer, sizeof(buffer), OPCODE, REG, IMM); EMIT
#define EMIT_REG16_IMM16(OPCODE, REG, IMM) \
  n = cg_x86_emit_reg16_imm16_instr(buffer, sizeof(buffer), OPCODE, REG, IMM); EMIT
#define EMIT_REG16_IMM8(OPCODE, REG, IMM) \
  n = cg_x86_emit_reg16_imm8_instr(buffer, sizeof(buffer), OPCODE, REG, IMM); EMIT
#define EMIT_REG32(OPCODE, REG) \
  n = cg_x86_emit_reg32_instr(buffer, sizeof(buffer), OPCODE, REG); EMIT
#define EMIT_REG32_IMM32(OPCODE, REG, IMM) \
  n = cg_x86_emit_reg32_imm32_instr(buffer, sizeof(buffer), OPCODE, REG, IMM); EMIT
#define EMIT_REG32_IMM8(OPCODE, REG, IMM) \
  n = cg_x86_emit_reg32_imm8_instr(buffer, sizeof(buffer), OPCODE, REG, IMM); EMIT
#define EMIT_REG8_REG8(OPCODE, REGA, REGB) \
  n = cg_x86_emit_reg8_reg8_instr(buffer, sizeof(buffer), OPCODE, REGA, REGB); EMIT
#define EMIT_REG16_REG16(OPCODE, REGA, REGB) \
  n = cg_x86_emit_reg16_reg16_instr(buffer, sizeof(buffer), OPCODE, REGA, REGB); EMIT
#define EMIT_REG32_REG32(OPCODE, REGA, REGB) \
  n = cg_x86_emit_reg32_reg32_instr(buffer, sizeof(buffer), OPCODE, REGA, REGB); EMIT
#define EMIT_REG8_MEM8(OPCODE, REG, BASE, SCALE, INDEX, DISPLACEMENT) \
  n = cg_x86_emit_reg8_mem8_instr(buffer, sizeof(buffer), OPCODE, REG, BASE, SCALE, INDEX, DISPLACEMENT); EMIT
#define EMIT_REG16_MEM16(OPCODE, REG, BASE, SCALE, INDEX, DISPLACEMENT) \
  n = cg_x86_emit_reg16_mem16_instr(buffer, sizeof(buffer), OPCODE, REG, BASE, SCALE, INDEX, DISPLACEMENT); EMIT
#define EMIT_REG32_MEM32(OPCODE, REG, BASE, SCALE, INDEX, DISPLACEMENT) \
  n = cg_x86_emit_reg32_mem32_instr(buffer, sizeof(buffer), OPCODE, REG, BASE, SCALE, INDEX, DISPLACEMENT); EMIT

static void log(const char *str) {
  fprintf(stderr, "%s\n", str);
}

static void *my_malloc(size_t n) {
  /* fprintf(stderr, "Allocating %d bytes\n", n); */
  return malloc(n);
}

static write_code(FILE *stream, char *buffer, int n) {
  if(n < 0) {
    fputs(cg_x86_get_last_error(), stderr);
    exit(1);
  }
  return fwrite(buffer, 1, n, stream);
}

int main(int argc, char **argv) {
  int n;
  char *buffer[1024];
  struct cg_x86_instr *instr;

  log("adc al, dl");
  EMIT_REG8_REG8(ADC,
		 AL,
		 DL);

  log("adc bx, cx");
  EMIT_REG16_REG16(ADC,
		   BX,
		   CX);

  log("adc eax, esi");
  EMIT_REG32_REG32(ADC,
		   EAX,
		   ESI);

  log("adc [ebp + 1 * esi], al");
  EMIT_MEM8_REG8(ADC,
		 EBP, 1, ESI, 0,
		 AL);

  log("adc [ebp + 2 * esi + 2], ax");
  EMIT_MEM16_REG16(ADC,
		   EBP, 2, ESI, 2,
		   AX);

  log("adc [ebp + 4 * esi - 4], eax");
  EMIT_MEM32_REG32(ADC,
		   EBP, 4, ESI, -4,
		   EAX);

  log("adc byte [ebp + esi], 0");
  EMIT_MEM8_IMM8(ADC,
		 EBP, 1, ESI, 0,
		 0);

  log("adc word [ebp + 2 * esi + 2], 0xffff");
  EMIT_MEM16_IMM16(ADC,
		   EBP, 2, ESI, 2,
		   0xffff);

  log("adc dword [ebp + 4 * esi - 4], 0xfafafafa");
  EMIT_MEM32_IMM32(ADC,
		   EBP, 4, ESI, -4,
		   0xfafafafa);

  log("adc ah, [ebx + 12]");
  EMIT_REG8_MEM8(ADC,
		 AH,
		 EBX, 0, 0, 12);

  log("adc di, [ebp + 4 * ecx + 12]");
  EMIT_REG16_MEM16(ADC,
		   DI,
		   EBP, 4, ECX, 12);

  log("adc edx, [ebp]");
  EMIT_REG32_MEM32(ADC,
		   EDX,
		   EBP, 0, 0, 0);

  log("adc bh, 42");
  EMIT_REG8_IMM8(ADC,
		 BH,
		 42);

  log("adc cx, 0x4242");
  EMIT_REG16_IMM16(ADC,
		   CX,
		   0x4242);

  log("adc edi, 0xdeadbeef");
  EMIT_REG32_IMM32(ADC,
		   EDI,
		   0xdeadbeef);

  log("adc cx, byte 42");
  EMIT_REG16_IMM8(ADC,
		  CX,
		  42);

  log("adc edi, byte 42");
  EMIT_REG32_IMM8(ADC,
		  EDI,
		  42);

  log("adc al, 0x66");
  EMIT_REG8_IMM8(ADC,
		 AL,
		 0x66);

  log("adc ax, 0x6666");
  EMIT_REG16_IMM16(ADC,
		   AX,
		   0x6666);

  log("adc eax, 0x66666666");
  EMIT_REG32_IMM32(ADC,
		   EAX,
		   0x66666666);

  log("add al, dl");
  EMIT_REG8_REG8(ADD,
		 AL,
		 DL);

  log("add bx, cx");
  EMIT_REG16_REG16(ADD,
		   BX,
		   CX);

  log("add eax, esi");
  EMIT_REG32_REG32(ADD,
		   EAX,
		   ESI);

  log("add [ebp + 1 * esi], al");
  EMIT_MEM8_REG8(ADD,
		 EBP, 1, ESI, 0,
		 AL);

  log("add [ebp + 2 * esi + 2], ax");
  EMIT_MEM16_REG16(ADD,
		   EBP, 2, ESI, 2,
		   AX);

  log("add [ebp + 4 * esi - 4], eax");
  EMIT_MEM32_REG32(ADD,
		   EBP, 4, ESI, -4,
		   EAX);

  log("add byte [ebp + esi], 0");
  EMIT_MEM8_IMM8(ADD,
		 EBP, 1, ESI, 0,
		 0);

  log("add word [ebp + 2 * esi + 2], 0xffff");
  EMIT_MEM16_IMM16(ADD,
		   EBP, 2, ESI, 2,
		   0xffff);

  log("add dword [ebp + 4 * esi - 4], 0xfafafafa");
  EMIT_MEM32_IMM32(ADD,
		   EBP, 4, ESI, -4,
		   0xfafafafa);

  log("add ah, [ebx + 12]");
  EMIT_REG8_MEM8(ADD,
		 AH,
		 EBX, 0, 0, 12);

  log("add di, [ebp + 4 * ecx + 12]");
  EMIT_REG16_MEM16(ADD,
		   DI,
		   EBP, 4, ECX, 12);

  log("add edx, [ebp]");
  EMIT_REG32_MEM32(ADD,
		   EDX,
		   EBP, 0, 0, 0);

  log("add bh, 42");
  EMIT_REG8_IMM8(ADD,
		 BH,
		 42);

  log("add cx, 0x4242");
  EMIT_REG16_IMM16(ADD,
		   CX,
		   0x4242);

  log("add edi, 0xdeadbeef");
  EMIT_REG32_IMM32(ADD,
		   EDI,
		   0xdeadbeef);

  log("add cx, byte 42");
  EMIT_REG16_IMM8(ADD,
		  CX,
		  42);

  log("add edi, byte 42");
  EMIT_REG32_IMM8(ADD,
		  EDI,
		  42);

  log("add al, 0x66");
  EMIT_REG8_IMM8(ADD,
		 AL,
		 0x66);

  log("add ax, 0x6666");
  EMIT_REG16_IMM16(ADD,
		   AX,
		   0x6666);

  log("add eax, 0x66666666");
  EMIT_REG32_IMM32(ADD,
		   EAX,
		   0x66666666);

  log("xor eax, eax");
  EMIT_REG32_REG32(XOR, EAX, EAX);
  log("inc eax");
  EMIT_REG32(INC, EAX);
  log("xor ebx, ebx");
  EMIT_REG32_REG32(XOR, EBX, EBX);
  log("or ebx, byte 42");
  EMIT_REG32_IMM8(OR, EBX, 42);
  log("int 0x80");
  EMIT_IMM8(INT, 0x80);

  return 0;
}
