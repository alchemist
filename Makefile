SUBDIRS = x86

lib :
	for dir in $(SUBDIRS); do (cd "$$dir" && $(MAKE) lib); done

all :
	for dir in $(SUBDIRS); do (cd "$$dir" && $(MAKE) all); done

clean :
	for dir in $(SUBDIRS); do (cd "$$dir" && $(MAKE) clean); done

distclean :
	for dir in $(SUBDIRS); do (cd "$$dir" && $(MAKE) distclean); done

doc :
	for dir in $(SUBDIRS); do (cd "$$dir" && $(MAKE) doc); done	

test :
	for dir in $(SUBDIRS); do (cd "$$dir" && $(MAKE) test); done

.PHONY : all clean distclean doc lib test
